#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

times convert(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.min = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.sec = atoi(str_number);
    return result;
}

void read(const char* file_name, marathon* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            marathon* item = new marathon;
            file >> item->number;
            file >> item->participant.last_name;
            file >> item->participant.first_name;
            file >> item->participant.middle_name;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(item->club, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}