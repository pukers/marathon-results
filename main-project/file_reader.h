#ifndef FILE_READER_H
#define FILE_READER_H

#include "marathon.h"

void read(const char* file_name, marathon* array[], int& size);

#endif