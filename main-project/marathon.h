#ifndef MARATHON_H
#define MARATHON_H

#include "constants.h"

struct person
{
    char last_name[MAX_STRING_SIZE];
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
};

struct times
{
    int hour;
    int min;
    int sec;
};

struct marathon
{
    int number;
    person participant;
    times start;
    times finish;
    char club[MAX_STRING_SIZE];
};

#endif