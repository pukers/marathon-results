#include "filter.h"
#include <cstring>
#include <iostream>

marathon** filter(marathon* array[], int size, bool (*check)(marathon* element), int& result_size)
{
	marathon** result = new marathon * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_results_by_club(marathon* element)
{
	return strcmp(element->club, "�������") == 0;
}

bool check_results_by_time(marathon* element)
{
	int start = element->start.hour * 60 * 60 + element->start.min * 60 + element->start.sec;
	int finish = element->finish.hour * 60 * 60 + element->finish.min * 60 + element->finish.sec;

	int total_sec = finish - start;

	return total_sec < 10200; // ���������� ������ � 2:50:00 ����
}
