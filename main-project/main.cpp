#include <iostream>
#include <iomanip>

using namespace std;

#include "marathon.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �7. ���������� ��������\n";
    cout << "�����: ������� ������\n";
    cout << "������: 14\n\n";
    marathon* marathon_results[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", marathon_results, size);
        cout << "***** ���������� �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ������ ��������� **********/
            cout << "�����...........: ";
            cout << marathon_results[i]->number << '\n';
            /********** ����� ��� ��������� **********/
            cout << "��������........: ";
            cout << marathon_results[i]->participant.last_name << ' ';
            cout << marathon_results[i]->participant.first_name << ' ';
            cout << marathon_results[i]->participant.middle_name << '\n';
            /********** ����� ������� ������ **********/
            cout << "�����...........: ";
            cout << setw(2) << setfill('0') << marathon_results[i]->start.hour << ':';
            cout << setw(2) << setfill('0') << marathon_results[i]->start.min << ':';
            cout << setw(2) << setfill('0') << marathon_results[i]->start.sec << '\n';
            /********** ����� ������� ������ **********/
            cout << "�����...........: ";
            cout << setw(2) << setfill('0') << marathon_results[i]->finish.hour << ':';
            cout << setw(2) << setfill('0') << marathon_results[i]->finish.min << ':';
            cout << setw(2) << setfill('0') << marathon_results[i]->finish.sec << '\n';
            /********** ����� ����� ��������� **********/
            cout << "����............: ";
            cout << marathon_results[i]->club << '\n';
            cout << '\n';
        }

        bool (*check_function)(marathon*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
                                                           // � ����������� � �������� ��������� �������� ���� book_subscription*
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ���������� ����� �������\n";
        cout << "2) ���������� �����, ��� 2:50:00\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_results_by_club; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ���������� ����� ������� *****\n\n";
            break;
        case 2:
            check_function = check_results_by_time; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ���������� �����, ��� 2:50:00 *****\n\n";
            break;
        default:
            throw "������������ ����� ������";
        }
        if (check_function)
        {
            int new_size;
            marathon** filtered = filter(marathon_results, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                /********** ����� ������ ��������� **********/
                cout << "�����...........: ";
                cout << filtered[i]->number << '\n';
                /********** ����� ��� ��������� **********/
                cout << "��������........: ";
                cout << filtered[i]->participant.last_name << ' ';
                cout << filtered[i]->participant.first_name << ' ';
                cout << filtered[i]->participant.middle_name << '\n';
                /********** ����� ������� ������ **********/
                cout << "�����...........: ";
                cout << setw(2) << setfill('0') << filtered[i]->start.hour << ':';
                cout << setw(2) << setfill('0') << filtered[i]->start.min << ':';
                cout << setw(2) << setfill('0') << filtered[i]->start.sec << '\n';
                /********** ����� ������� ������ **********/
                cout << "�����...........: ";
                cout << setw(2) << setfill('0') << filtered[i]->finish.hour << ':';
                cout << setw(2) << setfill('0') << filtered[i]->finish.min << ':';
                cout << setw(2) << setfill('0') << filtered[i]->finish.sec << '\n';
                /********** ����� ����� ��������� **********/
                cout << "����............: ";
                cout << filtered[i]->club << '\n';
                cout << '\n';
            }
            delete[] filtered;
        }

        for (int i = 0; i < size; i++)
        {
            delete marathon_results[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}